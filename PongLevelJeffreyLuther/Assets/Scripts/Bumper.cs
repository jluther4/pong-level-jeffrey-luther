﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {
    public GameObject RightBumper;
    public GameObject LeftBumper;
    public int speed = 7;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.W) && LeftBumper.transform.position.y < 4)
        {
            LeftBumper.transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S) && LeftBumper.transform.position.y > -4)
        {
            LeftBumper.transform.Translate(Vector3.down * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.UpArrow) && RightBumper.transform.position.y < 4)
        {
            RightBumper.transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.DownArrow) && RightBumper.transform.position.y > -4)
        {
            RightBumper.transform.Translate(Vector3.down * speed * Time.deltaTime);
        }
    }
}
