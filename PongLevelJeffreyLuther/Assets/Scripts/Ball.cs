﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    public float speed = 5f;
	// Use this for initialization
	void Start () {
        float StartX = Random.Range(0, 2) ;
        float StartY = Random.Range(0, 2) ;

        GetComponent<Rigidbody>().velocity = new Vector3(speed * StartX, speed * StartY, 0f);
    }
	
	// Update is called once per frame
	void Update () {
        
        
    }
}
